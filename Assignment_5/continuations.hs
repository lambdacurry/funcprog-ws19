data Expr a = Expr a :+: Expr a
            | Expr a :/: Expr a
            | Num Int
            | Var a
            | Let a Int (Expr a)
 deriving Show

eval :: Eq a => Expr a -> (a -> Maybe Int) -> (Int -> Maybe r) -> Maybe r
-- here we have two functions as argumetns in addition to our expression:
-- one is env - environment function for translating variables to ints
-- and other is cont -- for continuations
-- P.S. Eq a is needed for Let constructor

-- Num constructor

eval (Num n) _ cont = cont n

-- :+: constructor

eval (e1 :+: e2) env cont =
   eval e1 env $ \n1 ->
   eval e2 env $ \n2 ->
   cont (n1 + n2)

-- :/: constructor

eval (e1 :/: e2) env cont =
  eval e2 env $ \n2 ->
  case n2 of
    0 -> Nothing
    _ -> eval e1 env $ \n1 -> cont (n1 `div` n2)

-- Var constructor

eval (Var v) env cont = case env v of
  Nothing -> Nothing
  Just n  -> cont n

-- Let constructor

-- however I'm not sure this is a proper way how to solve it, but the idea is:
-- just go over all variables in expression e, if we find variable equal to our v, which
-- is going to be substituted, then we substitute it, else just evaluate as usual
eval (Let v n e) env cont = eval e (\var -> if var == v then Just n else env var) cont
