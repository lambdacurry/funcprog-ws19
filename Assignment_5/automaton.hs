data Automaton a = State Bool [(a, Automaton a)]

aStarbStar :: Automaton Char
aStarbStar = State True [('a', aStarbStar),('b', putB)] where
  putB :: Automaton Char
  putB = State True [('b', putB)]

getChList :: Automaton a -> [a]
getChList (State st [])      = []
getChList (State st (a:aut)) = fst b : getChList (State st aut)

checkWord :: Eq a => Automaton a -> [a] -> Bool
checkWord (State st _) []       = st
checkWord aut (x:xs)            = if x `elem` getChList aut
                                  then checkWord aut xs
                                  else False
-- Not sure whether this is right, but the idea is the following:
-- since we have a dfa, that means, that for each char we have either the
-- corresponding state(s) or this char is unacceptable by current automaton.
-- In order to check this I extract the list of all chars from all automaton's states
-- and if the head of the input word is in that list then I countinue recursively with tail,
-- if not, directly return False
-- 1st case just check whether the current state is final or not
