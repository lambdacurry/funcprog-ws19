data ShowExpr = Binary BinOp ShowExpr ShowExpr
              | Function Fun ShowExpr
              | Number Double

data BinOp = Plus | Div

data Fun = SquareRoot

instance Show (BinOp) where
  showsPrec p Plus = showString "+"
  showsPrec p Div  = showString "/"

instance Show (Fun) where
  showsPrec p SquareRoot = showString "sqrt"

instance Show (ShowExpr) where
  showsPrec p (Binary op e1 e2) = showString "(" . showsPrec p e1 . showsPrec p op .
                                  showsPrec p e2 . showString ")"
  showsPrec p (Function f (Number d))    = showString "(" . showsPrec p f .
                                  showString "(" . showsPrec p d . showString ")" .
                                  showString ")"
  showsPrec p (Function f e)    = showString "(" . showsPrec p f . showsPrec p e .
                                  showString ")"
  showsPrec p (Number d)        = showsPrec p d

testExpr :: ShowExpr
testExpr = Binary Div (Number 100) (Function SquareRoot (Binary Plus (Number 42) (Number 73)))

testExpr2 :: ShowExpr
testExpr2 = Function SquareRoot (Binary Div (Binary Div (Number 72.5) (Number 41.3)) (Function SquareRoot (Number 9)))
